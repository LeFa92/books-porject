import React from "react";
import Buttons from "../../../components/Buttons/Buttons";

const Book = (props) => {
  return (
    <>
      <td>{props.title}</td>
      <td>{props.author}</td>
      <td>{props.numberOfPage}</td>
      <td>
        <Buttons
          typeCss="btn-warning"
          handleClick={() => console.log("Modifier")}
        >
          Modifier
        </Buttons>
      </td>
      <td>
        <Buttons typeCss="btn-danger" handleClick={() => props.hundleDelet()}>
          Suppression
        </Buttons>
      </td>
    </>
  );
};

export default Book;
