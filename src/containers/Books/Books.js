import React, { Component } from "react";

import Book from "./Book/Book";
import Forms from "../../components/Fomrs/Forms";

class Books extends Component {
  state = {
    books: [
      {
        id: 1,
        title: "Le Petit Livre rouge",
        author: "Mao Tse-toung",
        numberOfPage: 200,
      },
      {
        id: 2,
        title: "DonQuichotte",
        author: "Miguel de Cervantes",
        numberOfPage: 320,
      },
      {
        id: 3,
        title: "Le Seigneur des anneaux",
        author: "J.R.R Tolkien",
        numberOfPage: 760,
      },
      {
        id: 4,
        title: "Harry Potter à l'école des sorciers",
        author: "J.K Rowling",
        numberOfPage: 420,
      },
    ],
    lastIdBook: 4,
  };
  hundleDeletBook = (id) => {
    const findIndexBook = this.state.books.findIndex((element) => {
      return element.id === id;
    });
    const newBooks = [...this.state.books];
    newBooks.splice(findIndexBook, 1);
    this.setState({ books: newBooks });
  };

  handleAddBook = (title, author, nbpage) => {
    const newBook = {
      id: this.state.lastIdBook + 1,
      title: title,
      author: author,
      numberOfPage: nbpage,
    };
    const newListBooks = { ...this.state.books };
    newListBooks.push(newBook);
    this.setState((oldState) => {
      return {
        books: newListBooks,
        lastIdBook: oldState.lastIdBook + 1,
      };
    });
  };

  render() {
    return (
      <>
        <table className="table text-center">
          <thead>
            <tr className="table-dark">
              <th>titre</th>
              <th>Auteur</th>
              <th>Nombre de page</th>
              <th colSpan="2"> Action </th>
            </tr>
          </thead>
          <tbody>
            {this.state.books.map((book) => {
              return (
                <tr key={book.id}>
                  <Book
                    title={book.title}
                    author={book.author}
                    numberOfPage={book.numberOfPage}
                    listOfBook={this.state.books}
                    hundleDelet={() => this.hundleDeletBook(book.id)}
                  />
                </tr>
              );
            })}
          </tbody>
        </table>
        {this.props.addFormBook && <Forms validation={this.handleAddBook} />}
      </>
    );
  }
}

export default Books;
