import React from "react";
import classes from "./Title.module.css";

const Title = (props) => {
  const myCss = `border border-dark p-2 mt-2 text-center bg-primary rounded ${classes.policeTitle}`;
  return <h1 className={myCss}>{props.children}</h1>;
};

export default Title;
