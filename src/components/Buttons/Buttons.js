import React from "react";

const Buttons = (props) => {
  const btnCss = `btn ${props.typeCss} ${props.widthCss}`;
  return (
    <button className={btnCss} onClick={props.handleClick}>
      {props.children}
    </button>
  );
};

export default Buttons;
