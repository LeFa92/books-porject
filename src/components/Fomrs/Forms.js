import React, { Component } from "react";

import Buttons from "../../components/Buttons/Buttons";

export class Forms extends Component {
  state = {
    titleCapture: "",
    authorCapture: "",
    numberPagesCapture: "",
  };

  handleValidateBook = (e) => {
    e.preventDefault();
    this.props.validation(
      this.state.titleCapture,
      this.state.authorCapture,
      this.state.numberPagesCapture
    );
  };
  render() {
    return (
      <>
        <h2
          style={{
            fontFamily: "Sigmar one",
            color: "rgb(23,124,179)",
            textAlign: "center",
          }}
        >
          Affichage du formulaire d'ajout
        </h2>

        <form>
          <div className="form-group">
            <label htmlFor="formGroupExampleInput">Titre du livre</label>
            <input
              type="text"
              className="form-control"
              id="formGroupExampleInput"
              placeholder=""
              value={this.state.titleCapture}
              onChange={(e) => this.setState({ titleCapture: e.target.value })}
            />
          </div>
          <div className="form-group">
            <label htmlFor="formGroupExampleInput2">Auteur</label>
            <input
              type="text"
              className="form-control"
              id="formGroupExampleInput2"
              placeholder=""
              value={this.state.authorCapture}
              onChange={(e) => this.setState({ authorCapture: e.target.value })}
            />
          </div>
          <div className="form-group">
            <label htmlFor="formGroupExampleInput2">Nombre de pages</label>
            <input
              type="text"
              className="form-control"
              id="formGroupExampleInput2"
              placeholder=""
              value={this.state.numberPagesCapture}
              onChange={(e) =>
                this.setState({ numberPagesCapture: e.target.value })
              }
            />
          </div>
          <Buttons typeCss="btn-primary" handleClick={this.handleValidateBook}>
            Valider
          </Buttons>
        </form>
      </>
    );
  }
}

export default Forms;
