import React, { Component } from "react";

import Title from "./components/Title/Title";
import Buttons from "./components/Buttons/Buttons";
import Books from "./containers/Books/Books";

import "./App.css";

class App extends Component {
  state = {
    addBook: false,
  };

  handleClickAddBook = () => {
    this.setState((oldState, props) => {
      return { addBook: !oldState.addBook };
    });
  };

  render() {
    return (
      <div className="container">
        <Title>Page listant les livres</Title>
        <Books addFormBook={this.state.addBook} />
        <Buttons
          widthCss="w-100"
          typeCss="btn-success"
          handleClick={this.handleClickAddBook}
        >
          {!this.state.addBook ? "Ajouter" : "Fermer l'ajout"}
        </Buttons>
      </div>
    );
  }
}

export default App;
